# Injecting runtime parameters

When writing your tests, a good practice is to avoid hard coding data which may change
depending on the environment that your test is running in. An example of this might be
the base url to use: locally it might be `http://localhost:8080` but once you've finished
building your test and you want to run it against a production or staging environment
you may need to change that url to `https://staging.yourwebsite.com` but keep the rest
of the test the same. 

Stamphpede provides two methods to inject variables on a per environment basis both 
use the config.yaml file.

## Hard coding parameters

The simplest way to inject parameters is to set them directly in the config file, you
could then deploy a different config file per environment along with your tests. An 
example for injecting via config is

```yaml
parameters:
    base_url: "http://localhost:8080"
    admin_user: "admin"
    admin_password: "Password1!"
``` 

## Using environment variables

A more advanced method to inject parameters is to use environment variables. This
allows you to keep a single config file for all environments but to change the 
values dynamically per environment.

```yaml
parameters:
    base_url: !env LOAD_TEST_URL
    admin_user: "admin"
    admin_password: !env LOAD_TEST_ADMIN_PASSWORD
``` 

You can mix values from the environment with hard coded values.

Additionally, if there is a .env file present Stamphpede will use variables from
this file to overwrite any defined environment vars; this is mainly intended as a
development aid making it easier to develop tests and commit their default values
into source control

## Accessing injected parameters 

For a test case to access parameters, you need to define a method to receive them 
called 'injectParameters' this method takes a single argument which is a Symfony
ParameterBag class

```php
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

return new class () {
    private string $url;
    
    public function injectParameters(ParameterBag $params): void
    {
        $this->url = (string) $params->get('base_url');
    }
    
    // ... define your tasks here
};
```

Each key defined in the config file under the parameters key will be present in the
Parameter Bag for you to access and setup your class with. The injectParameters method
will be called as part of parsing your test case so it will always get called before
any tasks are executed.

### Naming tasks

Stamphpede reserves method names beginning with inject for future enhancements to the
parameter and dependency injection feature. Any task methods beginning with inject 
will be ignored by the parser.
