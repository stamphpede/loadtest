# Loading test data

You will often find yourself in the position that your load tests need a data set, 
perhaps a very large data set, in order to run. At the current time, Stamphpede does
not provide methods for loading this data into your system or for making it accessible
for your tests. These are the methods that we recommend using to handle data sets 
currently.

## Use your application

By far the best route to creating data that your tests need to operate is to create
that data as part of the test. You can use dependencies between tests to ensure that,
for example a user is created, before you try to login as them. This created data is
private to a single **Elephpant** and is supplied via method parameters to dependent 
**Tasks**

## Use your CI pipeline

Stamphpede is intended to be one tool amongst many that you use while building CI 
pipelines for your application, so we don't plan on supporting use cases which are 
better served by a different tool. If you find yourself in a situation where you need
to load a large amount of data (maybe you are testing search performance with 10k 
products) and it is impractical to load it in via your application (perhaps bulk 
creation isn't a feature in your application and single creation is too slow to be
used in this scenario) we recommend using a dedicated step in your CI pipeline which
runs prior to your test which loads your data. This could be as simple as reloading a
database backup or could be a more complex script to programmatically create the data.

Accessing data that has been created (eg a list of valid usernames) in this manner is a 
bit more tricky, at the moment we don't have any supported mechanisms for loading a 
data set (eg from a csv) containing your data references. Until this is available the
suggested approach is to generate the data in such a way that it fits a pattern and
leverage this in your tests. For example, you could create user accounts with the same
(strong) password and usernames matching a pattern of "load_test_user_%d" with %d a 
number between 1000 and 2000. In your test you can then pick a random number between
1000 and 2000 and use that to derive a user account to log in with.

It is possible to use the inject data functionality to pass in data, however this 
function is intended to be used for passing configuration data not for passing large
data sets. In the above example, it would be appropriate to pass in the user password
and pattern for generating usernames but passing a list of 1000 users would not be
recommended.   
