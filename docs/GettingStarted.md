

# Getting started with Stamphpede

Stamphpede provides you with a toolset for creating load test scenarios which you can use to test your own web
sites. It should go without saying that you should only use these tools on sites you control, or you have the
permission of the owner (and in some cases, the hosting company) as this tool has the ability to overload a 
website and make it unavailable for a period of time. When running your tests, we suggest that you start with
a low level of load and increase it gradually to test the capabilities of your site.

## Read first

- Installation ?
- Using CLI tool ?

## Terminology

Throughout this guide, and the documentation the following terms are used:

**Elephpant** An elephpant represents a user visiting your site, it has **Tasks** to carry out which will 
result in it making **Requests** as it proceeds upon it's **Journey** through your website.

**Task** A task consists of a **Request** to make along with some metadata to control how frequently and
in what order an **Elephpant** will execute it.

**Request** A request is a single HTTP(s) request made to a site. The load test runner will record stats about
each request to help you evaluate the success of the test once it is finished.

**Journey** An **Elephpant** will make a journey through your website. **Tasks** will be picked at random 
based on their associated metadata and subject to the rules defined later on.

**Stamphpede** A Stamphpede is a representation of a load test. It consists of a **Herd** of **Elephpants** 
along with some metadata controlling things such as the length of the run.

**Herd** A Herd is a group of **Elephpants** they will all be trying to use your website at the same time
 *gulp* 
 
**Stamphpede Runtime** The Stamphpede runtime is the tool which runs your **Stamphpedes** it uses PHP 7.4
and is intended to be run as a stand alone tool using Docker. This allows you to write your tests using PHP
7.4 syntax without requiring PHP 7.4 elsewhere in your infrastructure. The drawback of this is that you are
limited as to the code you can write - for example you cannot bring in any new composer dependencies for your
tests.
 
## Your first Stamphpede
 
A **Stamphpede** is defined in it's own file by an anonymous class as follows.  

```php
<?php

return new class() {

};
```

If you save this into a new .php file you will be able to run this using the cli tool. @TODO link to how to run
You will notice that this is a very sedentary **Stamphpede** and nothing much happens. In order to actually
test something, we also need to define some **Tasks**

### Your first task

**Tasks** are defined as methods on your **Stamphpede** class and are flagged as such to the runtime using an
Annotation. You should import the annotations using `use Stamphpede\Annotation as ST;` then we can modify our
**Stamphpede** class as follows:

```php
<?php

use Stamphpede\Annotation as ST;

return new class() {
    /** @ST\Task("view-index") */
    public function viewIndex()
    {
    
    }
};
```

Running this with the CLI tool will result in an error as each task is expected to return a **Request** to
run. Let's make this change.

### Making a request

All **Requests** should be defined as PSR-7 request objects, we include the RingCentral PSR-7 library for you 
to use for this. You should modify your task to return a request object like so.

```php
<?php

use Stamphpede\Annotation as ST;
use \RingCentral\Psr7\Request;

return new class() {
    /** @ST\Task("view-index") */
    public function viewIndex()
    {
        return new Request('GET', 'http://localhost:8080');
    }
};
```

Provided you have a webserver running locally on port 8080 when you run the above **Stamphpede** you will
get some results out. 

## Customising the Stamphpede

Our very first **Stamphpede** was very simple, it created a **Herd** of 2 **Elephpants** which ran for 30
seconds. If you are testing a website which usually gets a lot of traffic you will probably want to do 
more than this. The **Stamphpede Runtime** provides you with a few configurable options here, which can
be set via annotations {In future these will become defaults which can be overridden per run} 

There are three annotations you can define on a **Stamphpede** class to control how it runs

```php
<?php

use Stamphpede\Annotation as ST;

/**
 * @ST\Duration(60)
 * @ST\Concurrency(10)
 * @ST\RampUp(10)
 */
return new class() {
    //...
};
```

This example shows the annotations 

**Duration** controls how long the **Stamphpede** will run for in seconds,
if set to 60 as in the above example the **Stamphpede Runtime** will stop making new **Requests** after 60
seconds, however the test may still continue for a while longer - until any in progress requests have finished.

**Concurrency** controls how many **Elephpants** are created as part of your **Herd** The **Stamphpede Runtime**
will initially create this number and will replace any **Elephpants** which finish their **Journey** to keep
the **Herd** size at this level.

{Future option: control if finished **Elephpants** are replaced}

{Future option: control starting **Herd** size and target **Herd** size after ramp up}

**RampUp** is the number of seconds over which the **Stamphpede Runtime** will create new **Elephpants** in
order to reach the desired **Herd** size specified by **Concurrency**. {At present this hasn't been implemented}
  
## More complex tasks

**Elephpants** are smart creatures, there is far more they can do than just bash away at a single webpage
but in order to do so, we need to look at the other annotations that are available for our **Tasks**

### Task Weighting

Once you define more than one **Task** in your **Stamphpede**, every time an **Elephpant** finishes a **Request**
it will choose a new **Task** to execute at random from all it's available **Tasks**. By default there is an
equal chance that it will choose any of your defined **Tasks** however this isn't always desirable. If you 
are correctly modeling real world usage of your site, some pages will see a greater number of requests than
others, that is to say requests to your website aren't equally distributed across ever page. An **Elephpant**
can be instructed to take this into account using the **Weight** Annotation.

```php
<?php

use Stamphpede\Annotation as ST;
use \RingCentral\Psr7\Request;

return new class() {
    /** @ST\Task("view-index") */
    public function viewIndex()
    {
        return new Request('GET', 'http://localhost:8080');
    }
    
    /**
      * @ST\Task("view-busy-page")
      * @ST\Weight(3)
      */
    public function viewBusyPage()
    {
        return new Request('GET', 'http://localhost:8080/busy-page');
    }
};
```

In the above **Stamphpede** the url `busy-page` will be visited 3 times more than the `index` page (which has
a default weight of 1)

### Request Caching

By default, the **Stamphpede Runtime** will only call a **Task** method once per **Elephpant** and will 
cache the returned **Request** sometimes you won't want it to do this, should it be depending on having
the freshest results possible when generating the request or if you are determining how to build a request
at random. For this purpose there is a **Cacheable** annotation you can use to indicate that the **Task**
method should be call each time.

```php
<?php

use Stamphpede\Annotation as ST;
use \RingCentral\Psr7\Request;

return new class() {
    /** 
      * @ST\Task("view-product") 
      * @ST\Cacheable(false)
      */
    public function viewProduct()
    {
        //view a random product
        $productId = rand(1, 500);
        return new Request('GET', 'http://localhost:8080/product/' . $productId);
    }
};
```

### Dependent tasks

So far we've only made `GET` requests and have not needed any data, there are many instances where this isn't
sufficient so **Requests** can be made dependent on each other. When a **Request** is dependent on another,
it will not be executed until after the **Elephpant** has made at least one **Request** to the dependency in
addition, you have the option of being passed the result of that **Request** as a parameter to your **Task**

```php
<?php

use Stamphpede\Annotation as ST;
use \RingCentral\Psr7\Request;
use \Stamphpede\Response;

return new class() {
    /** @ST\Task("login") */
    public function login()
    {
        return new Request('POST', 'http://localhost:8080/login', [], '{"username":"Ele","password":"secret"}');
    }
    
    /**
      * @ST\Task("view-dashboard")
      * @ST\Depends("login")
      */
    public function viewDashboard(Response $response)
    {
        $responseBody = $response->getBodyFromJson();
        $authToken = $responseBody->authToken;

        return new Request('GET', 'http://localhost:8080/dashboard', ['X-AuthToken' => $authToken]);
    }
};
```

In the example above, we use the **Depends** annotation to give the viewDashboard method a dependency on the
login **Task**. When defining a dependency, you should use the **Task** name; the name of the method defining
the **Task** is ignored. 

In this example, we only have one dependency, so we type hint on the class `Stamphpede\Response` this class is
a wrapper around a PSR-7 response with a few helper methods to make developing tasks easier. You can always
get the original response object by calling `$response->getPsrResponse()`

A **Task** is not limited to a single dependency, you can define as many dependencies as you need but be 
careful if you define a set of **Tasks** with circular dependencies the **Elephpants** **Journey** will be  
terminated if that **Task** is selected for execution.

When a **Task** has a dependency on multiple other **Requests** you can retain the type hint of `Stamphpede\Response`
however there is no guarantee as to which of the responses you are dependent on will be passed into the method.
In this instance, you are much better off type hinting on `Stamphpede\ResponseCollection` which is a basic 
collection object containing all the responses from your dependent **Requests**.  

```php
<?php

use Stamphpede\Annotation as ST;
use \RingCentral\Psr7\Request;
use \Stamphpede\Response;
use \Stamphpede\ResponseCollection;

return new class() {
    /** @ST\Task("login") */
    public function login()
    {
        return new Request('POST', 'http://localhost:8080/login', [], '{"username":"Ele","password":"secret"}');
    }
    
    /**
      * @ST\Task("view-dashboard")
      * @ST\Depends("login")
      */
    public function viewDashboard(Response $response)
    {
        $responseBody = $response->getBodyFromJson();
        $authToken = $responseBody->authToken;

        return new Request('GET', 'http://localhost:8080/dashboard', ['X-AuthToken' => $authToken]);
    }
    /**
      * @ST\Task("view-chart")
      * @ST\Depends("login")
      * @ST\Depends("view-dashboard")
      */
    public function viewChart(ResponseCollection $responseCollection)
    {
        $loginResponse = $responseCollection->get("login");
        $responseBody = $loginResponse->getBodyFromJson();
        $authToken = $responseBody->authToken;
        
        $dashboardResponse = $responseCollection->get("view-dashboard");
        $responseBody = $dashboardResponse->getBodyFromJson();
        $chartId = $responseBody->chartId;

        return new Request('GET', 'http://localhost:8080/chart/' . $chartId, ['X-AuthToken' => $authToken]);
    }

};
```

Be careful if you store any state in your **Stamphpede** class, this class is shared by all **Elephpants**
so by extension any state you store can be accidentally used or overwritten by a different **Elephpant** to
the one you intended it for. It is possible to use this to communicate between different **Elephpants** in
your **Herd** however we **strongly caution against** this approach as it will lead to some very interesting bugs
should you ever run your **Stamphpede** over multiple servers or should we make changes to the **Stamphpede Runtime**
which better isolates individual **Elephpants** from each other.

#### Behaviour of dependent tasks

Dependent tasks complicate the logic for choosing a **Request** as they cannot be executed until all their
dependencies have been met. The following rules decide how a **Task** gets selected:

1. All the **Weights** of all the **Tasks** in a **Stamphpede** are added together, regardless of dependencies
2. A **Task** is selected with probability **Weight**/sum(**Weights**) 
3. If a **Task** has an unmet dependency, select one of the dependencies instead
4. Repeat (3) until a valid **Task** has been found.

This has the effect of increasing the **Weight** of **Tasks** that other **Tasks** are dependent on, until 
they have been executed at least once. If you make a small run with lots of interdependent **Tasks** this 
could skew your results.

### Control the Journey with Repeatable

By default, every task can be executed an infinate number of times. If you want to define a **Journey** which
has a set number of steps (eg a checkout flow) or you want to better model user interaction (eg a user
will only login once per session) there is a need to stop certain tasks from being repeated. You can control
the repeatability of a **Task** using the **Repeatable** annotation. 

{Future work: Add a MaxRepeats Annotation to allow finer grained control over number of repeats}

A basic usage of **Repeatable** might be to skip the login request, notice the import of `Stamphpede\Parser\Task`
so that we can use it's constants in our annotations.

```php
<?php

use Stamphpede\Annotation as ST;
use Stamphpede\Parser\Task;
use \RingCentral\Psr7\Request;
use \Stamphpede\Response;

return new class() {
    /** 
      * @ST\Task("login") 
      * @ST\Repeatable(ST\Repeatable::REPEAT_SKIP)
      */
    public function login()
    {
        return new Request('POST', 'http://localhost:8080/login', [], '{"username":"Ele","password":"secret"}');
    }
    
    /**
      * @ST\Task("view-dashboard")
      * @ST\Depends("login")
      */
    public function viewDashboard(Response $response)
    {
        $responseBody = $response->getBodyFromJson();
        $authToken = $responseBody->authToken;

        return new Request('GET', 'http://localhost:8080/dashboard', ['X-AuthToken' => $authToken]);
    }
};
```

There are four allowed values for the annotation: `Repeatable::REPEAT_OK`, `Repeatable::REPEAT_END`, `Repeatable::REPEAT_IGNORE`
and `Repeatable::REPEAT_SKIP`. OK is the default and will allow infinite repeating, End is used to finish an **Elephpants**
**Journey** and spawn a new one; Ignore and Skip currently behave approximately the same but will differ once
the functionality of the **Stamphpede Runtime** is completed. The essential difference is that Ignore will
cause the **Elephpant** to immediately choose a different **Task** to execute, whereas Skip will cause the
**Stamphpede Runtime** to put the **Elephpant** back on the execution queue and come back to it later. 

{Future work will introduce the ability to configure a Pause time between executing **Tasks**, Skip will 
incur that Pause time, while Ignore will not}

### Advanced Journeys with Repeatable

In the previous section, we discussed the possiblity of creating a step by step **Journey** for example
to simulate a checkout process, there are several ways you could achieve this here is an example for a 
three step process.

```php
use Stamphpede\Annotation as ST;
use Stamphpede\Parser\Task;
use \RingCentral\Psr7\Request;
use \Stamphpede\Response;

return new class() {
    /**
      * @ST\Task("step1")
      * @ST\Weight(0)
      * @ST\Repeatable(ST\Repeatable::REPEAT_IGNORE)
      */
    public function step1()
    {
      //...
    }
    /**
      * @ST\Task("step2")
      * @ST\Weight(0)
      * @ST\Depends("step1")
      * @ST\Repeatable(ST\Repeatable::REPEAT_IGNORE)
      */
    public function step2()
    {
      //...
    }
    /**
      * @ST\Task("step3")
      * @ST\Depends("step2")
      * @ST\Repeatable(ST\Repeatable::REPEAT_END)
      */
    public function step3()
    {
      //...
    }
};
```

To explain what is going on here:

Step 1 defines a weight of 0 and Repeat Ignore. The weight of 0 means that it will never be selected on 
it's own. (This is a performance trick, if you have lots of steps or lots of step by step definitions it
will take an **Elephpant** a long while to pick a valid next **Task** so we exclude this **Task** from 
consideration) 

Step 2 also defines a weight of 0 and Repeat Ignore but also a dependency on Step 1.

Step 3 defines a dependency on Step 2 and Repeat End.

When the **Elephpant** goes to select a **Task** it will always land upon Step 3, since they other two steps
have 0 **Weight** the first time it hits it, it will be unable to execute it as it depends on Step 2 which
hasn't been executed yet, so it'll try to execute Step 2. This is also impossible as Step 2 depends on Step 1,
so the **Elephpant** falls back on executing Step 1. 

The next time an **Elephpant** goes to select a **Task** a similar thing will happen, however in this instance
it will be able to execute Step 2 as it has now done Step 1. As we have set Step 1's weight to 0, the repeat
ignore is not strictly required here as it will never be picked up other than as a dependency of Step 2,
however it would be required should the **Weight** be anything other than 0.

The third time an **Elephpant** goes to select a **Task** it picks Step 3 as usual, but it's now possible to
execute it, so it will.

The final time an **Elephpant** goes to select a **Task** it again picks Step 3, however as it's set to repeat
End, the **Journey** is over and the **Elephpant** makes no further requests. The **Stamphpede Runtime** will
recreate it with a fresh set of Requests so it can start the process over again.

## Read next

Setting up a server ?
Using it in CI ?
