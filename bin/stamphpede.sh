#!/usr/bin/env bash

NETWORK="stamphpede_network"
CLIENT_IMAGE="registry.gitlab.com/stamphpede/client:0"
SERVER_IMAGE="registry.gitlab.com/stamphpede/server:0"
SERVER="stamphpede_server"
CLIENT_VOLUME=""
SERVER_VOLUME=""
SERVER_FLAGS=""

function ensure_server() {
  docker container inspect $SERVER > /dev/null 2>&1 || \
    docker create -v `pwd`/.stamphpede:/stamphpede $SERVER_VOLUME --name $SERVER --network=$NETWORK $SERVER_IMAGE serve $SERVER_FLAGS
}

function ensure_server_running() {
  [ "$( docker container inspect -f '{{.State.Status}}' $SERVER )" == "running" ] || docker container start $SERVER
}

function ensure_network() {
  docker network inspect $NETWORK > /dev/null 2>&1 || \
    docker network create $NETWORK
}

function init() {
    docker run --rm -v `pwd`/.stamphpede:/stamphpede $CLIENT_IMAGE generate-config /stamphpede
    start_server
}

function start_server() {
    ensure_network
    ensure_server
    ensure_server_running
}

function stop_server() {
    if [ "$( docker container inspect -f '{{.State.Status}}' $SERVER )" == "running" ]; then
      docker container stop $SERVER
    fi;

    docker container rm $SERVER > /dev/null 2>&1
}

function connect() {
    docker network connect $1 $SERVER
}

function run() {
    docker run --rm -v `pwd`/testcases:/testcases -v `pwd`/.stamphpede:/stamphpede $CLIENT_VOLUME --network=$NETWORK $CLIENT_IMAGE run "$@"
}

function build_server() {
    stop_server

    $(
      cd ../server;
      DOCKER_BUILDKIT=1 docker build -t $SERVER_IMAGE:latest .
    )
}

function build_client() {
    $(
      cd ../client;
      DOCKER_BUILDKIT=1 docker build -t $CLIENT_IMAGE:latest .
    )
}

if [ ! -d .stamphpede ]; then
  echo "Stamphpede directory not found. Are you sure you ran this command from the root of a stamphpede project?";
  exit 1;
fi;

if [ -f .stamphpede/DEV ]; then
  if [ -d ../client ]; then
    CLIENT_VOLUME="-v `pwd`/../client/:/opt/project"
    CLIENT_IMAGE="registry.gitlab.com/stamphpede/client"
  fi;

  if [ -d ../server ]; then
    SERVER_VOLUME="-v `pwd`/../server/:/opt/project"
    SERVER_IMAGE="registry.gitlab.com/stamphpede/server"
  fi;

  SERVER_FLAGS="-vvv"

  case "$1" in
  build_server)
    if [ ! -d ../server ]; then
      echo "Server repository not found in expected location, please clone the server code into $(realpath `pwd`/../server)"
      exit 1;
    fi;

    build_server
    exit 0
    ;;

    build_client)
    if [ ! -d ../client ]; then
      echo "Client repository not found in expected location, please clone the server code into $(realpath `pwd`/../client)"
      exit 1;
    fi;

    build_client
    exit 0
    ;;
  esac
fi;

case "$1" in
init)
  if [ "$2" == '-h' ]; then
    echo "Usage: stamphpede init"
    echo "Creates config files with default settings, starts the server. Running again will overwrite and changes you've made to config";
    exit 1
  fi;

  init
  exit 0
  ;;

start_server)
  if [ "$2" == '-h' ]; then
    echo "Usage: stamphpede start_server"
    echo "Starts the server. No-op if server is already running. Will restart a stopped container by preference.";
    exit 1
  fi;

  start_server
  exit 0
  ;;

stop_server)
  if [ "$2" == '-h' ]; then
    echo "Usage: stamphpede stop_server"
    echo "Stops the server. No-op if server is not running. Will also remove the container.";
    exit 1
  fi;

  stop_server
  exit 0
  ;;

restart_server)
  if [ "$2" == '-h' ]; then
    echo "Usage: stamphpede restart_server"
    echo "Restarts server and recreates docker container. Will need to reconnect networks after running.";
    exit 1
  fi;

  stop_server
  start_server
  exit 0
  ;;

connect)
  if [ "$2" == '-h' ]; then
    echo "Usage: stamphpede connect <network>"
    echo "Connects the server to a docker network, use this to link stamphpede with your local development environment for testing";
    exit 1
  fi;

  shift
  connect "$@"
  exit 0
  ;;

run)
  if [ "$2" == '-h' ]; then
    echo "Usage: stamphpede run <path_to_file>"
    echo "Runs a test case and prints the results. The ./testcases directory is mounted at the root of the client container, so treat ./ as /";
    echo "Example: stamphpede run /testcases/testcase.php"
    exit 1
  fi;

  shift
  run "$@"
  exit 0
  ;;

*)
  echo "Usage: stamphpede {init|run|start_server|connect}"
  echo "Use the -h flag to get help with individual commands"
  exit 1
  ;;

esac
