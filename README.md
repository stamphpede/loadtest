# Stamphpede quickstart project

This repository exists as a quickstart to get you writing Stamphpede load tests as 
quickly as possible. 

### Prerequisites 

You will need docker installed locally (ideally configured to allow the 
current user to run docker commands without sudo)

You will either need your app running locally inside docker or a pre-provisioned test server
that you can connect to. 

### Getting started

##### Step 1:

`composer create-project stamphpede/loadtest`

##### Step 2:

`./stamphpede init`

##### Step 3:

Write your test, see docs folder for a walk through guide. Put it inside the ./testcases directory.

##### Step 4:

If running against a local environment you may need to connect the stamphpede server to your project:

`./stamphpede connect <network_name>`

You can find all the networks using `docker network ls` if you are using docker-compose for your project
then by default the network name will be <project>_default. Don't forget, when referencing containers from
other networks you need to add the network name to their host eg if you want to access the docker-compose
service api in the stamphpede project with the default network name you would reference it as 
`api.stamphpede_default`

##### Step 5

`./stamphpede run /testcases/yourtestcase.php`

Notice the use of an absolute path when referencing the test case: this is the location the testcases
folder is mounted inside the docker container running stamphpede. 
